import re, sequtils, strutils
import elvis
import words

const
  nounsRaw = """
    tomo tawa = vehicle
    ilo = tool
    jan = person/people
    jo = property/properties
    kili = fruit/fruit
    lili = small thing
    meli = woman/women
    mi = @I/we/me/us
    mije = man/men
    moku = food
    pona = good
    suli = big thing
    telo = liquid
    toki = language
    tomo = house
    """
  verbsTrRaw = """
    jo = have/has/had
    lili = decrease
    moku = eat
    suli = increase
    telo = water
    toki = say/says/said
    """
  verbsInRaw = """
    kili = be fruit
    lili = be small
    meli = be female
    mije = be male
    moku = be food
    pona = be good
    suli = be big
    telo = flow
    toki = talk
    tomo = be a house
    """

let
  nounPattern = re"(.+?) = ([^/]+)(?:/([^/]+))?(?:/([^/]+))?(?:/([^/]+))?"
  verbPattern = re"(.+?) = ([^/]+)(?:/([^/]+))?(?:/([^/]+))?"
  copulaPattern = re"be (.*)"

proc parseNoun(line: string): Noun =
  if line.strip =~ nounPattern:
    result.tp = matches[0]
    result.enSg = matches[1]
    result.enPl = matches[2] ?: matches[1] & "s"
    result.enSgAcc = matches[3] ?: result.enSg
    result.enPlAcc = matches[4] ?: result.enPl
    if result.enSg.startsWith("@"):
      result.enSg = result.enSg[1..^1]
    else:
      result.article = true
  else:
    raise newException(ValueError, "Invalid noun")

proc parseVerb(line: string): Verb =
  if line.strip =~ verbPattern:
    result.tp = matches[0]
    if matches[1] =~ copulaPattern:
      result.en1S = "am " & matches[0]
      result.en3S = "is " & matches[0]
      result.enPl = "are " & matches[0]
      result.enPastSg = "was " & matches[0]
      result.enPastPl = "were " & matches[0]
    else:
      result.en1S = matches[1]
      result.en3S = matches[2] ?: matches[1] & "s"
      result.enPl = matches[1]
      result.enPastSg = matches[3] ?: (matches[1][^1] == 'e' ? matches[1] & "d" ! matches[1] & "ed")
      result.enPastPl = result.enPastSg
  else:
    raise newException(ValueError, "Invalid verb")

let
  nouns* = nounsRaw.strip.splitLines.map(parseNoun)
  verbsTr* = verbsTrRaw.strip.splitLines.map(parseVerb)
  verbsIn* = verbsInRaw.strip.splitLines.map(parseVerb)
