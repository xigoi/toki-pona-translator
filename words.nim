type
  Noun* = object
    tp*: string
    enSg*: string
    enPl*: string
    enSgAcc*: string
    enPlAcc*: string
    article*: bool
  NounForm* = enum
    nfSg
    nfPl
    nfSgAcc
    nfPlAcc
  Verb* = object
    tp*: string
    en1S*: string
    en3S*: string
    enPl*: string
    enPastSg*: string
    enPastPl*: string
  VerbForm* = enum
    vf1S
    vf3S
    vfPl
    vfPastSg
    vfPastPl

func `[]`*(noun: Noun, form: NounForm): string =
  case form
  of nfSg: noun.enSg
  of nfPl: noun.enPl
  of nfSgAcc: noun.enSgAcc
  of nfPlAcc: noun.enPlAcc

func `[]`*(verb: Verb, form: VerbForm): string =
  case form
  of vf1S: verb.en1S
  of vf3S: verb.en3S
  of vfPl: verb.enPl
  of vfPastSg: verb.enPastSg
  of vfPastPl: verb.enPastPl
